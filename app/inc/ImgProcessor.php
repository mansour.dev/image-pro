<?php


namespace app\inc;

use svay\FaceDetector;
use Fpp\Client;

class ImgProcessor{

    public function file_get_contents_curl($url) {

        $ch = new CurlRequest($url);
        $ch->setOption(CURLOPT_HEADER,0);
        $ch->setOption(CURLOPT_RETURNTRANSFER,1);
        $ch->setOption(CURLOPT_URL,$url);
        $data = $ch->execute($ch);

        try {
            if (!$ch->getImageSize($url)) {
                throw new \Exception('Not valid image');
            }

        }catch(\Exception $e) {
            return false;
        }
        $ch->close();

        return $data;
    }

    /**
     * add image data to the database
     *
     * @param string $imageName - The image filename
     * @param boolean $resized - true/false value
     */


    protected function insert_image_data($imageName,$resized,$is_valid){
        $mysql = new DBClass();
        $mysql->insert('images', array('image_path' => $imageName, 'resized' => $resized,'is_valid_url'=>$is_valid));

    }

    public function process_images(){

      $sources = [
          "https://hugbeta.com/tests/int-01-images/13333.jpg",
          "https://hugbeta.com/tests/int-01-images/2.jpg",
          "https://hugbeta.com/tests/int-01-images/3.jpeg",
          "https://hugbeta.com/tests/int-01-images/4.png",
          "https://hugbeta.com/tests/int-01-images/5.bmp",
          "https://hugbeta.com/tests/int-01-images/6.jpg"
      ];


        // Image path
        $dir = dirname( dirname(__FILE__) );
        $save_to =  $dir.'/images/original/';
        $resize_path = $dir.'/images/resized/';

        foreach ($sources as $source){
           $imageName = basename($source);

           // check if image exist
           $exist = $this->image_exist($imageName);
           if($exist == false){

               //download image
               $data = $this->file_get_contents_curl($source);

               // Check if is valid image
               if ($data !== false){
                   file_put_contents( $save_to.$imageName, $data );

                   // Resize the image to exact size
                   $resize = new ImgResize($save_to.$imageName);
                   $resize->resizeTo(250, 400, 'exact');
                   $resize->saveImage($resize_path.$imageName);

                   // insert image date to the database
                   $this->insert_image_data($imageName,TRUE,TRUE);
               }else{
                   $this->insert_image_data($imageName,0,0);

               }

               //$this->face_detect($save_to.$imageName,$imageName);
           }

        }


    }

    /**
     * Check if the image processed before
     *
     * @param string $imageName - The image filename
     */

    public function image_exist($imageName){
        $mysql = new DBClass();
        $images = $mysql->where('image_path',$imageName)->get('images');
        if(!empty($images)){
            return true;
        }else{
            return false;
        }

    }


    // get all images that already processed

    public function get_all_images(){
        $mysql = new DBClass();
        $images = $mysql->get('images');

        if (!empty($images)){

            return $images;
        }else{
            return "No Images yet!";
        }

    }

    public function face_detect($path,$imageName){
        $detector = new FaceDetector('detection.dat');
        $detector->faceDetect($path);
        $dir = dirname( dirname(__FILE__) );
        $save_to = $dir. '/images/resized/';
        $cropped = $detector->cropFaceToJpeg($save_to.$imageName);
        return $cropped ;

    }

    public function faceplus(){


        $apiKey = 'asVl_Fi0nw1SED0UFm--XQjKovDlnR5p';
        $apiSecret = 'EIVxQCubXCUGH2CRhoyzW6Y5pJmPWsKg';

        $host = 'https://api-us.faceplusplus.com';


        $client = new Client($apiKey, $apiSecret, $host);

        $data = array(
            'image_url' => "https://hugbeta.com/tests/int-01-images/4.png",
            'return_attributes' => 'headpose'
        );

        $data = array(
            'face_tokens' => "d8683ecf3521171a2409af65fc911baa",
            'user_data' => ''
        );

        //$resp = $client->detectFace($data);
        $resp = $client->createFaceset($data);


        return $resp;
    }

    public function test (){

        $dir = dirname( dirname(__FILE__) );
        $path = $dir.'/images/original/2.jpg';

        $imageName ='test2.jpg';

        $result = $this->face_detect($path,$imageName);



    }


}